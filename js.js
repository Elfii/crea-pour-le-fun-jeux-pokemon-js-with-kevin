let evoli = {
  x: 36,
  y: 9,
  z: 0,
  element: {} ,
  color : "orange",
  s: 32,
  image: "https://cdn.shopify.com/s/files/1/0252/3362/1040/files/gif_evoli_09e8b56c-6dc3-4131-9b2a-734c6d95bad7_480x480.gif?v=1585237563" ,
    
  sound: function(){
      let audio = new Audio("son/evoli.ogg");
      audio.play();
    },

  bouge: function(x,y){
    
    if(this.x + x < 0 || this.x + x > 39 || this.y + y < 0 || this.y + y > 19){
      return; // ne pas bouger si la position est en dehors de l'écran
    }
    if(this.x + x === ponyta.x && this.y + y === ponyta.y){
      return; // n'exécutera pas la fonction bouge car les conditions du return sont respectés
    }
    this.x += x  
    this.y += y
    // changer le translate
    this.element.style.transform = `translate(${this.x * this.s}px, ${this.y * this.s}px)` 
    this.element.style.zIndex = this.y;
    this.element.style.position = "absolute";
  },
  
  evo: function(){ 
  const container = document.getElementById("container");   
    this.element = document.createElement("div");
    this.element.style.width = "100px";
    this.element.style.height = "100px";
    this.element.style.backgroundImage = `url(${this.image})`;
    this.element.style.backgroundSize = "contain";
    this.element.style.backgroundRepeat = "no-repeat";
    this.element.style.transform = `translate(${this.x * this.s}px, ${this.y * this.s}px)`;

    
  container.appendChild(this.element);
    
  document.addEventListener("keydown", function(event) {
    if (event.keyCode === 81) { // Flèche gauche = Q
      this.bouge(-1, 0);
    } else if (event.keyCode === 90) { // Flèche haut = Z
      this.bouge(0, -1); 
    } else if (event.keyCode === 68) { // Flèche droite = D
      this.bouge(1, 0);
    } else if (event.keyCode === 83) { // Flèche bas = S
      this.bouge(0, 1);
    } else if (event.keyCode === 65) { // Touche A
      this.sound();
    }
    }.bind(this));  
  },
};



let ponyta = {
  x: 0,
  y: 9,
  z: 0,
  element: {} ,
  color : "jaune",
  s: 32,
  image: "https://custom-doodle.com/wp-content/uploads/doodle/pokemon-ponyta/pokemon-ponyta-doodle.gif" ,

  sound: function(){
    let audio = new Audio("son/ponyta.ogg");
    audio.play();
  },

  bouge: function(x,y){
  
    if(this.x + x < 0 || this.x + x > 39 || this.y + y < 0 || this.y + y > 18){
      return; // ne pas bouger si la position est en dehors de l'écran
    }
    if(this.x + x === evoli.x && this.y + y === evoli.y){
      return; // n'exécutera pas la fonction bouge car les conditions du return sont respectés
    }
    this.x += x
    this.y += y
    // changer le translate
    this.element.style.transform = `translate(${this.x * this.s}px, ${this.y * this.s}px)`
    this.element.style.zIndex = this.y;
    this.element.style.position = "absolute";  
  },
  pony: function(){
  
  const container = document.getElementById("container");
    
    this.element = document.createElement("div");
    this.element.style.width = "100px";
    this.element.style.height = "100px";
    this.element.style.backgroundImage = `url(${this.image})`;
    this.element.style.backgroundSize = "contain";
    this.element.style.backgroundRepeat = "no-repeat";
    this.element.style.transform = `translate(${this.x * this.s}px, ${this.y * this.s}px)`;

  container.appendChild(this.element);
    
  document.addEventListener("keydown", function(event) {
    if (event.keyCode === 37) { // Flèche gauche
      this.bouge(-1, 0);
    } else if (event.keyCode === 38) { // Flèche haut
      this.bouge(0, -1);
    } else if (event.keyCode === 39) { // Flèche droite
      this.bouge(1,0);
    } else if (event.keyCode === 39){
      this.bouge(1,0);
   }else if (event.keyCode === 40) { // Flèche bas
      this.bouge(0, 1);
    } else if (event.keyCode === 220) { // Touche Etoile 
      this.sound();
    }
    }.bind(this));  
  },
};


evoli.evo();
ponyta.pony();



